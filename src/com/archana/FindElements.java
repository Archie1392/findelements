/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.archana;

/**
 *
 * @author Archana
 */
public class FindElements {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        int[] arr={1,2,3,4,5};
        findElements(arr, 6);
    }

    public static boolean findElements(int arr[], int sum) {

        for (int i = 0; i < arr.length - 2; i++) {
            for (int j = i + 1; j < arr.length - 1; j++) {
                for (int k = j + 1; k < arr.length; k++) {
                    if (arr[i] + arr[j] + arr[k] == sum) {
                        System.out.println("3 elements are  "+arr[i]+" , "+arr[j]+" , "+arr[k]);
                        return true;
                    }
                }
            }
        }
        System.out.println("There are no 3 elements which can make sum = "+sum);
        return false;
    }

}
